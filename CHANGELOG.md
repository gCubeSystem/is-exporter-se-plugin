This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Information System Exporter Smart Executor Plugin


## [v2.0.0-SNAPSHOT]

- Ported plugin to smart-executor APIs 3.0.0 [#21617]
- Switched JSON management to gcube-jackson [#19116]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Refactored code to support IS Model reorganization (e.g naming, packages)
- Refactored code to support renaming of Embedded class to Property [#13274]
- Starting to use the new Encrypted property type [#12812]
- Updated smart-executor-bom


## [v1.3.0] [r4.13.0] - 2018-11-20

- Using resource-registry 2.0.0 APIs [#11949]


## [v1.2.0] [r4.9.0] - 2017-12-20

- Creating uber-jar instead of jar-with-dependencies and using new make-servicearchive directive [#10159]
- Using new APIs signature [#10318]


## [v1.1.0] [r4.6.0] - 2017-07-25

- Removed List and Set usage as workaround for OrientDB bug [#9021]


## [v1.0.0] [r4.5.0] - 2017-06-07

- First Release [#7666]


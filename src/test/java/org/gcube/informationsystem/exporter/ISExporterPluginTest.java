package org.gcube.informationsystem.exporter;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.informationsystem.exporter.mapper.GenericResourceExporterTest;
import org.gcube.informationsystem.exporter.mapper.ServiceEndpointExporterTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ISExporterPluginTest {
	
	private static Logger logger = LoggerFactory.getLogger(ISExporterPluginTest.class);
	
	// @Test
	public void testLaunch() throws ObjectNotFound, Exception {
		String[] contexts = {
			"/gcube", "/gcube/devNext", "/gcube/devNext/NextNext" //, "/gcube/devsec", "/gcube/devsec/devVRE"
		};
		
		for(String context : contexts) {
			logger.info("\n\n\n-------------------------------------------------------------------------");
			ContextTest.setContextByName(context);
			
			ISExporterPlugin isExporterPlugin = new ISExporterPlugin();
			Map<String,Object> inputs = new HashMap<String,Object>();
			inputs.put(ISExporterPlugin.FILTERED_REPORT, true);
			inputs.put(ISExporterPlugin.STATISTICS, false);
			isExporterPlugin.launch(inputs);
			logger.info("\n\n\n");
		}
	}
	
	@Test
	public void export() throws ObjectNotFound, Exception {
		String[] contexts = {
				// "/gcube", "/gcube/devNext", 
				"/gcube/devNext/NextNext" //, 
				// "/gcube/devsec", "/gcube/devsec/devVRE"
		};
		
		for(String context : contexts) {
			ContextTest.setContextByName(context);
			
			GenericResourceExporterTest genericResourceExporterTest = new GenericResourceExporterTest();
			genericResourceExporterTest.export();
			
			ServiceEndpointExporterTest serviceEndpointExporterTest = new ServiceEndpointExporterTest();
			serviceEndpointExporterTest.export();
		}
	}
	
	//@Test
	public void removeExported() throws ObjectNotFound, Exception {
		String[] contexts = {
				"/gcube", "/gcube/devNext", "/gcube/devNext/NextNext", "/gcube/devsec", "/gcube/devsec/devVRE"
		};
		
		for(String context : contexts) {
			ContextTest.setContextByName(context);
			
			GenericResourceExporterTest genericResourceExporterTest = new GenericResourceExporterTest();
			genericResourceExporterTest.removeExported();
			
			ServiceEndpointExporterTest serviceEndpointExporterTest = new ServiceEndpointExporterTest();
			serviceEndpointExporterTest.removeExported();
		}
	}
}

package org.gcube.informationsystem.exporter.mapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import org.gcube.informationsystem.exporter.ContextTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCoreResourceMapperTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(GenericResourceExporterTest.class);
	
	protected File getReportFile(GCoreResourceMapper<?,?> grm) throws Exception {
		String contextName = GCoreResourceMapper.getCurrentContextName();
		File file = grm.getReportFile(GenericResourceExporterTest.class, contextName, Calendar.getInstance());
		String json = "{}";
		synchronized (file) {
			try(FileWriter fw = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)){
					out.println(json);
					out.flush();
			} catch( IOException e ){
			   throw e;
			}
		}
		return file;
	}
	
	//@Test
	public void testWorkspace() throws Exception {
		
		String[] contexts = {
				"/gcube", "/gcube/devNext", "/gcube/devNext/NextNext" //, "/gcube/devsec", "/gcube/devsec/devVRE"
		};
		
		for(String context : contexts) {
			logger.info("\n\n\n-------------------------------------------------------------------------");
			ContextTest.setContextByName(context);
			GenericResourceExporter gre = new GenericResourceExporter(false, false);
			File file = getReportFile(gre);
			logger.info("\n\n\n {}", file);
			/*
			gre.publishFileToWorkspace(file);
			logger.info("\n\n\n");
			file.delete();
			*/
		}
		
	}
	
}

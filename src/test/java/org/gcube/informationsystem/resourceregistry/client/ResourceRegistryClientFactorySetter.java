package org.gcube.informationsystem.resourceregistry.client;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryClientFactorySetter {

	public static void forceToURL(String url) {
		ResourceRegistryClientFactory.forceToURL(url);
	}

}

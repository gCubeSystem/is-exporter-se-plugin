package org.gcube.informationsystem.resourceregistry.publisher;

import org.gcube.informationsystem.exporter.ContextTest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceRegistryPublisherFactorySetter extends ContextTest {

	public static void forceToURL(String url) {
		ResourceRegistryPublisherFactory.forceToURL(url);
	}
	
}

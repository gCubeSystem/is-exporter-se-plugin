package org.gcube.informationsystem.exporter.mapper;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Profile;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.resources.gcore.ServiceEndpoint.Runtime;
import org.gcube.common.resources.gcore.common.Platform;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.model.impl.properties.HeaderImpl;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.Header;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.StateFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ServiceEndpointExporter extends GCoreResourceMapper<ServiceEndpoint,EService> {
	
	private static Logger logger = LoggerFactory.getLogger(ServiceEndpointExporter.class);
	
	public static final String FIXED_VERSION = "1.0.0";
	
	public static final String PLATFORM = "PLATFORM";
	public static final String POSITION = "POSITION";
	
	public static final String GHN_ID = "ghnID";
	
	public static final String USERNAME = "USERNAME";
	public static final String PASSWORD = "PASSWORD";
	
	public static final String NAME = "name";
	public static final String VALUE = "value";
	
	public static final String ARRAY = "ARRAY";
	public static final String PROPERTIES = "PROPERTIES";
	
	public static final String NOT_URI_ENDPOINT = "NOT_URI_ENDPOINT";
	
	public ServiceEndpointExporter(boolean filteredReport, Boolean statistics) {
		super(ServiceEndpoint.class, EService.class, filteredReport, statistics);
	}
	
	@Override
	protected EService map(ServiceEndpoint gr) throws Exception {
		Profile profile = gr.profile();
		UUID uuid = UUID.fromString(gr.id());
		boolean readFromIS = false;
		
		EService eService = null;
		AccessPointFacet[] accessPointFacets = null;
		SoftwareFacet softwareFacet = null;
		SoftwareFacet platformSoftwareFacet = null;
		StateFacet stateFacet = null;
		NetworkingFacet networkingFacet = null;
		
		try {
			readFromIS = resourceRegistryClient.existInstance(rClass, uuid);
		} catch(NotFoundException e) {
			readFromIS = false;
		} catch(AvailableInAnotherContextException e) {
			resourceRegistryPublisher.addResourceToCurrentContext(EService.NAME, uuid, false);
			Thread.sleep(100);
			readFromIS = true;
		}
		
		Group<AccessPoint> accessPoints = profile.accessPoints();
		accessPointFacets = new AccessPointFacet[accessPoints.size()];
		
		if(readFromIS) {
			eService = read(uuid);
			//softwareFacet = (SoftwareFacet) eService.getIdentificationFacets().get(0);
			
			List<ConsistsOf<? extends Resource,? extends Facet>> consistsOfs = eService.getConsistsOf();
			
			for(ConsistsOf<? extends Resource,? extends Facet> c : consistsOfs) {
				Facet target = c.getTarget();
				if(c instanceof IsIdentifiedBy) {
					if(target instanceof SoftwareFacet) {
						softwareFacet = (SoftwareFacet) target;
						continue;
					}
				} else {
					if(target instanceof AccessPointFacet) {
						try {
							Object positionObject = c.getAdditionalProperty(POSITION);
							Integer position = Integer.valueOf(positionObject.toString());
							if(position != null) {
								accessPointFacets[position] = (AccessPointFacet) target;
							}
						} catch(Exception e) {
							// Position is used on relation to match the AccessPoint on ServiceEndpoint
							logger.error("No POSITION found", e);
						}
						continue;
					}
					
					if(target instanceof SoftwareFacet) {
						SoftwareFacet targetSoftwareFacet = (SoftwareFacet) target;
						if(targetSoftwareFacet.getGroup().compareTo(PLATFORM) == 0) {
							platformSoftwareFacet = targetSoftwareFacet;
						}
						continue;
					}
					
					if(target instanceof StateFacet) {
						stateFacet = (StateFacet) target;
						continue;
					}
					
					if(target instanceof NetworkingFacet) {
						networkingFacet = (NetworkingFacet) target;
						continue;
					}
					
				}
				
			}
		} else {
			eService = new EServiceImpl();
			Header header = new HeaderImpl(uuid);
			eService.setHeader(header);
		}
		
		/* ----------------------------------------- */
		if(softwareFacet == null) {
			softwareFacet = new SoftwareFacetImpl();
			IsIdentifiedBy<EService,SoftwareFacet> identifiedBy = new IsIdentifiedByImpl<EService,SoftwareFacet>(
					eService, softwareFacet, null);
			eService.addFacet(identifiedBy);
		}
		
		softwareFacet.setGroup(profile.category());
		softwareFacet.setName(profile.name());
		softwareFacet.setVersion(FIXED_VERSION);
		String description = profile.description();
		if(description != null && description.compareTo("") != 0) {
			softwareFacet.setDescription(getStringAsUTF8(description));
		}
		/* ----------------------------------------- */
		
		/* ----------------------------------------- */
		Platform platform = profile.platform();
		
		if(platformSoftwareFacet == null) {
			platformSoftwareFacet = new SoftwareFacetImpl();
			eService.addFacet(platformSoftwareFacet);
		}
		
		platformSoftwareFacet.setGroup(PLATFORM);
		platformSoftwareFacet.setName(platform.name());
		String platformVersion = String.format("%d.%d.%d-%d", platform.version(), platform.minorVersion(),
				platform.revisionVersion(), platform.buildVersion());
		softwareFacet.setVersion(platformVersion);
		/* ----------------------------------------- */
		
		/* ----------------------------------------- */
		Runtime runTime = profile.runtime();
		
		if(stateFacet == null) {
			stateFacet = new StateFacetImpl();
			eService.addFacet(stateFacet);
		}
		stateFacet.setValue(runTime.status());
		
		if(networkingFacet == null) {
			networkingFacet = new NetworkingFacetImpl();
			eService.addFacet(networkingFacet);
		}
		networkingFacet.setHostName(runTime.hostedOn());
		String ghnID = runTime.ghnId();
		if(ghnID != null && ghnID.compareTo("") != 0) {
			networkingFacet.setAdditionalProperty(GHN_ID, ghnID);
		}
		
		/* ----------------------------------------- */
		
		/* ----------------------------------------- */
		int i = 0;
		for(AccessPoint accessPoint : accessPoints) {
			if(accessPointFacets[i] == null) {
				accessPointFacets[i] = new AccessPointFacetImpl();
				ConsistsOf<EService,AccessPointFacet> consistsOf = new ConsistsOfImpl<EService,AccessPointFacet>(
						eService, accessPointFacets[i], null);
				consistsOf.setAdditionalProperty(POSITION, i);
				eService.addFacet(consistsOf);
			}
			
			accessPointFacets[i].setEntryName(accessPoint.name());
			String address = accessPoint.address();
			if(address != null && address.compareTo("") != 0) {
				try {
					URI uri = URI.create(address);
					accessPointFacets[i].setEndpoint(uri);
				} catch(IllegalArgumentException e) {
					accessPointFacets[i].setAdditionalProperty(NOT_URI_ENDPOINT, address);
				}
			}
			
			String accessPointDescription = accessPoint.description();
			if(accessPointDescription != null && accessPointDescription.compareTo("") != 0) {
				accessPointFacets[i].setDescription(getStringAsUTF8(accessPointDescription));
			}
			
			/* Managing Username and Password */
			try {
				String username = accessPoint.username();
				String passwordString = accessPoint.password();
				Encrypted password = new EncryptedImpl();
				password.setEncryptedValue(passwordString);
				accessPointFacets[i].setAdditionalProperty(USERNAME, username);
				accessPointFacets[i].setAdditionalProperty(PASSWORD, password);
			} catch(NullPointerException e) {
				
			}
			/* END Managing Username and Password */
			
			/* Managing AccessPoint Properties */
			Group<Property> properties = accessPoint.properties();
			if(properties != null && properties.size() > 0) {
				/* 
				 * List/Set support is currently disabled due to OrientDB bug see https://github.com/orientechnologies/orientdb/issues/7354
				 *
				ObjectNode propertiesObjectNode = ISMapper.getObjectMapper().createObjectNode();
				// This is a workaround
				ArrayNode arrayNode = propertiesObjectNode.putArray(ARRAY);
				
				for(Property property : properties) {
					ObjectNode objectNode = ISMapper.getObjectMapper().createObjectNode();
					objectNode.put(NAME, property.name());
					boolean encryptedValue = property.isEncrypted();
					if(encryptedValue) {
						Encrypted encrypted = new EncryptedImpl();
						encrypted.setEncryptedValue(property.value());
						objectNode.put(VALUE, encryptedValue);
					} else {
						objectNode.put(VALUE, property.value());
					}
					
					arrayNode.add(objectNode);
				}
				
				accessPointFacets[i].setAdditionalProperty(PROPERTIES, propertiesObjectNode);
				*/
				
				for(Property property : properties) {
					String name = property.name();
					boolean encryptedValue = property.isEncrypted();
					if(encryptedValue) {
						Encrypted encrypted = new EncryptedImpl();
						encrypted.setEncryptedValue(property.value());
						accessPointFacets[i].setAdditionalProperty(name, encryptedValue);
					} else {
						accessPointFacets[i].setAdditionalProperty(name, property.value());
					}
				}
			}
			/* END Managing AccessPoint Properties */
			
			i++;
		}
		/* ----------------------------------------- */
		
		return eService;
	}
	
}
